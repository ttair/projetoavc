﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {
    private int pontuacao = 0;
    [SerializeField]
    private GameObject avatar;
    [SerializeField]
    private GameObject[] meshsDoAvatar;
    // Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //Gets e Sets

    public void setPontuacao (int pts)
    {
        pontuacao = pts;
        if (pontuacao < 0)
        {
            pontuacao = 0;
        }
        gameObject.GetComponent<CanvasManager>().setScoreText(pontuacao);

    }

    public int getPontuacao()
    {
        return pontuacao;
    }
    
    public GameObject getAvatar()
    {
        return avatar;
    }

    public GameObject[] getMeshsDoAvatar()
    {
        return meshsDoAvatar;
    }
}
