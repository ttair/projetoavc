﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AlvosManager : MonoBehaviour {
    [SerializeField]
    private GameObject[] alvos;
    [SerializeField]
    private Transform[] posicoes;
    private List<GameObject> listaDeAlvos = new List<GameObject>();
    [SerializeField]
    private CanvasManager canvasManager;

    // Use this for initialization
    void Start () {
        listaDeAlvos.AddRange(alvos);
    }
	
	// Update is called once per frame
	void Update () {
	
	}


    public GameObject sorteiaAlvo()
    {   
        return listaDeAlvos[Random.Range(0, listaDeAlvos.Count)];
        //return alvos[Random.Range(0, alvos.Length)];
    }

    public Transform sorteiaPosicao ()
    {
        return posicoes[Random.Range(0, posicoes.Length)];
    }


    public IEnumerator destruirAlvo (GameObject alvo)
    {
        canvasManager.mostraPontosFeitos(true);
        canvasManager.getPontosFeitos().animation.Play("PtsSubindo");
        GetComponent<PlayerManager>().setPontuacao(GetComponent<PlayerManager>().getPontuacao() + 10);
        for (float i = 1; i >= 0; i -= Time.deltaTime)
        {
            alvo.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0, i);
            //Debug.Log("Teste: " + i);
        }

        yield return new WaitForSeconds(1.5f);
        GetComponent<ModoPegar>().AlvoAtualDestruido = true;
        listaDeAlvos.Remove(alvo);
        Destroy(alvo);


    }

    public IEnumerator instanciarAlvo (GameObject alvo, Transform posi)
    {
        alvo.SetActive(true);
        alvo.transform.position = posi.transform.position;
        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            alvo.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, i);
            
        }
        yield return null;
    }

    //Gets e Sets

    public List<GameObject> getAlvos()
    {
        return listaDeAlvos;
    }
}
