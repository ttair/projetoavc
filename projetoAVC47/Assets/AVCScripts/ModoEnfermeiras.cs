﻿using UnityEngine;
using System.Collections;

public class ModoEnfermeiras : MonoBehaviour {
    [SerializeField]
    private GameObject[] arrayDeEnfermeiras;
    [SerializeField]
    private Material enf;
    [SerializeField]
    private Material enfVazio;
    [SerializeField]
    private CanvasManager canvasManager;
    [SerializeField]
    private PlayerManager playerManager;
    [SerializeField]
    private int vezesParaAparecerEnfermeiras;
    private int cont = 0;

    // Use this for initialization
    void Start () {
        canvasManager = GetComponent<CanvasManager>();
        canvasManager.setIndicadorDeLevel("LEVEL 2");
        canvasManager.setFalaDoutorText("VI UMA ENFERMEIRA LINDA HOJE. VOCÊ PODE TOCAR NA TELA QUANDO ELA APARECER ENTRE ESSAS PESSOAS PARA QUE EU POSSA ENCONTRÁ-LA?");
        for (int i = 0; i < arrayDeEnfermeiras.Length; i++)
        {
            arrayDeEnfermeiras[i].SetActive(false);
        }

	    for (int i = 0; i < arrayDeEnfermeiras.Length; i++)
        {
            tirarEnfermeira(arrayDeEnfermeiras[i]);
        }
        StartCoroutine(mostrarEnf());
        colocarEnfermeira(escolherPosi());
        
	}
	
	// Update is called once per frame
	void Update () {
	    if (cont >= vezesParaAparecerEnfermeiras)
        {
            mostrarTodasAsEnfs();
            //CanvasManager.mostraPassouDeLevel(true);
            StartCoroutine(CanvasManager.acabarFase());
            cont = -1;
        }
	}

    public void tirarEnfermeira(GameObject posi)
    {
        posi.GetComponent<Renderer>().material = enfVazio;
        posi.GetComponent<BoxCollider>().enabled = false;
    }

    public void colocarEnfermeira (GameObject posi)
    {
        posi.GetComponent<Renderer>().material = enf;
        posi.GetComponent<BoxCollider>().enabled = true;
    }

    public GameObject escolherPosi()
    {
        return arrayDeEnfermeiras[Random.Range(0, arrayDeEnfermeiras.Length)];
    }
    public void mostrarTodasAsEnfs()
    {
        for (int i = 0; i < arrayDeEnfermeiras.Length; i++)
        {
            arrayDeEnfermeiras[i].GetComponent<Renderer>().material = enf;
            arrayDeEnfermeiras[i].GetComponent<BoxCollider>().enabled = false;
        }
    }

    public Material getEnf()
    {
        return enf;
    }

    public Material getEnfVazio()
    {
        return enfVazio;
    }

    public GameObject[] ArrayDeEnfermeiras
    {
        get
        {
            return arrayDeEnfermeiras;
        }

        set
        {
            arrayDeEnfermeiras = value;
        }
    }

    public int getVezesParaAparecerEnfermeiras
    {
        get
        {
            return vezesParaAparecerEnfermeiras;
        }

        set
        {
            vezesParaAparecerEnfermeiras = value;
        }
    }

    public IEnumerator mostrarEnf()
    {
        yield return new WaitForSeconds(3.5f);
        for (int i = 0; i < arrayDeEnfermeiras.Length; i++) {
            yield return new WaitForSeconds(0.5f);
            arrayDeEnfermeiras[i].SetActive(true);
        }
    }

    public int Cont
    {
        get
        {
            return cont;
        }

        set
        {
            cont = value;
        }
    }

    public CanvasManager CanvasManager
    {
        get
        {
            return canvasManager;
        }

        set
        {
            canvasManager = value;
        }
    }

    public PlayerManager PlayerManager
    {
        get
        {
            return playerManager;
        }

        set
        {
            playerManager = value;
        }
    }
}
