﻿using UnityEngine;
using System.Collections;


public class ColisaoManagerEnf : MonoBehaviour
{
    [SerializeField]
    private ModoEnfermeiras modoEnfermeiras;
    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Colisão Detectada: " + collision.gameObject.name);

        if ((collision.gameObject.tag == "MEsq") || (collision.gameObject.tag == "MDir"))
        {
            Debug.Log("Colisão: " + collision.collider.tag);
            //Debug.Log("Material: " + GetComponent<Renderer>().material + " :: " + modoEnfermeiras.getEnf().name);
            Debug.Log("Colisão Detectada NA ENF: " + collision.gameObject.name);
            //modoEnfermeiras.CanvasManager.mostraPontosFeitos(true);
            modoEnfermeiras.PlayerManager.setPontuacao(modoEnfermeiras.PlayerManager.getPontuacao() + 10);
            modoEnfermeiras.tirarEnfermeira(gameObject);
            modoEnfermeiras.colocarEnfermeira(modoEnfermeiras.escolherPosi());
            modoEnfermeiras.Cont++;
        }
    }
}
