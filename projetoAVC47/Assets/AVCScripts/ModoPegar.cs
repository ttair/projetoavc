﻿using UnityEngine;
using System.Collections;

public class ModoPegar : MonoBehaviour {

    private bool alvoAtualDestruido = true;
    private CanvasManager canvasManager;
    private AlvosManager alvosManager;

    

    // Use this for initialization
    void Start () {
        canvasManager = GetComponent<CanvasManager>();
        alvosManager = GetComponent<AlvosManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (alvosManager.getAlvos().Count == 0)
        {
            StartCoroutine(canvasManager.acabarFase());
        } else{
            if (alvoAtualDestruido) {
                Debug.Log("Instanciando Alvo... Quatidade na lista: " + alvosManager.getAlvos().Count);
                StartCoroutine (alvosManager.instanciarAlvo(alvosManager.sorteiaAlvo(), alvosManager.sorteiaPosicao()));
                alvoAtualDestruido = false;
            }
        }
	}

    public bool AlvoAtualDestruido
    {
        get
        {
            return alvoAtualDestruido;
        }

        set
        {
            alvoAtualDestruido = value;
        }
    }
}
