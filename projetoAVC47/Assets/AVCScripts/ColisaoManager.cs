﻿using UnityEngine;
using System.Collections;


public class ColisaoManager : MonoBehaviour {
    public AlvosManager alvosManager;
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Colisão Detectada: " + collision.gameObject.name);
        if ((collision.gameObject.tag == "MEsq") || (collision.gameObject.tag == "MDir"))
        {
            StartCoroutine(alvosManager.destruirAlvo(this.gameObject));
        }
    }
}
