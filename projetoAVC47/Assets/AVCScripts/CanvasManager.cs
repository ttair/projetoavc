﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour {
    [SerializeField]
    private GameObject doutor;

    [SerializeField]
    private GameObject score;

    [SerializeField]
    private Image pontosFeitos;

    [SerializeField]
    private Text indicadorDeLevel;

    [SerializeField]
    private Image passouDeLevel;

    private PlayerManager playerManager;
    

	// Use this for initialization
	void Start () {
        playerManager = GetComponent<PlayerManager>();
        mostraDoutor(true);
        mostraScore(false);
        mostraAvatar(false);
        mostraPontosFeitos(false);
        StartCoroutine(ComecarFase(3));
        
    }
	
	// Update is called once per frame
	void Update () {

	}

    public void mostraPontosFeitos (bool status)
    {
        pontosFeitos.gameObject.SetActive(status);
    }

    public void mostraIndicadorDeLevel (bool status)
    {
        indicadorDeLevel.gameObject.SetActive(status);
    }

    public void mostraDoutor(bool status)
    {
        doutor.SetActive(status);
    }

    public void mostraScore (bool status)
    {
        score.SetActive(status);
    }

    public void mostraPassouDeLevel(bool op)
    {
        passouDeLevel.gameObject.SetActive(op);
        StartCoroutine(FadeImage(!op, passouDeLevel));
    }

    public void mostraAvatar (bool op) 
    {
        if (op)
        {
            for (int i = 0; i < playerManager.getMeshsDoAvatar().Length; i++)
            {
                playerManager.getMeshsDoAvatar()[i].layer = 5;
            }
        } else
        {
            for (int i = 0; i < playerManager.getMeshsDoAvatar().Length; i++)
            {
                playerManager.getMeshsDoAvatar()[i].layer = 1;
            }
        }
        
    }

    public IEnumerator FadeImage(bool fadeAway, Image img)
    {
        if (fadeAway)
        {
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                img.color = new Color(1, 1, 1, i);
                yield return null;
                
            }
        }

        else
        {
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }

    IEnumerator FadeText(bool fadeAway, Text text)
    {
        if (fadeAway)
        {
            for (float i = 1; i >= 0; i -= Time.deltaTime)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, i);
                yield return null;

            }
        }

        else
        {
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, i);
                yield return null;
            }
        }
    }

    public IEnumerator ComecarFase(int tempo)
    {
        yield return new WaitForSeconds(tempo);
        StartCoroutine(FadeText(true, indicadorDeLevel));
        yield return new WaitForSeconds(1.5f);
        mostraAvatar(true);
        mostraScore(true);
    }

    public IEnumerator acabarFase ()
    {
        setFalaDoutorText("PARABÉNS! AGORA VAMOS PARA A PRÓXIMA FASE!");
        mostraPassouDeLevel(true);
        yield return new WaitForSeconds(3.5f);
        Debug.Log("Nome do lvl: " + Application.loadedLevelName);
        if (Application.loadedLevelName == "LEVEL1")
        {
            Application.LoadLevel("LEVEL2");
        }

        if (Application.loadedLevelName == "LEVEL2")
        {
            Application.LoadLevel("LEVEL1");
        }
    }

    //Gets e Sets
    public void setFalaDoutorText (string texto)
    {
        doutor.GetComponentInChildren<Text>().text = texto;
    }

    public string getFalaDoutorText ()
    {
        return doutor.GetComponentInChildren<Text>().text;
    }
    public Image getPontosFeitos()
    {
        return pontosFeitos;
    }
    public void setScoreText(int score)
    {
        mostraPontosFeitos(true);
        pontosFeitos.animation.Play();
        StartCoroutine(FadeImage(false, pontosFeitos));
        this.score.GetComponentInChildren<Text>().text = score.ToString();
    }

    public int getScoreText()
    {
        return int.Parse(score.GetComponentInChildren<Text>().text);
    }

    public void setIndicadorDeLevel(string texto)
    {
        indicadorDeLevel.text = texto;
    }

    public string getIndicadorDeLevel()
    {
        return indicadorDeLevel.text;
    }
}
